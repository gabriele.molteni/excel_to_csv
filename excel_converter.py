import os
import pandas as pd
from pathlib import Path
from os import listdir
from os.path import isfile, join

try:
    os.mkdir('.\csv')
except OSError:
    print("Creation of the csv directory failed")
else:
    print("Successfully created the csv directory")

#Read all files in the excel folder
excelfiles = [f for f in listdir('.\excel') if isfile(join('.\excel', f))]

#Strip all newline in the excel file
for i in excelfiles:
    df = pd.read_excel('.\excel\\' + i)
    df = df.replace(r'\n', ' ', regex = True).replace('\r', ' ', regex = True)
    df.to_csv('.\csv\\' + Path(i).stem +'.csv', header = False, index = False, sep = ';')
    print('Created '+ Path(i) + ' csv')

